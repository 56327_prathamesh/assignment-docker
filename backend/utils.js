function createResult(error, data) {
  const result = {};
  if (error) {
    // if there is any error
    result["status"] = "error";
    // result.status = 'error'

    result["error"] = error;
    // result.error = error
  } else {
    // there is no error (success)
    result["status"] = "success";
    result["data"] = data;
  }

  return result;
}
// const mysql = require("mysql2");

// const openConnection = () => {

  // const openConnection = mysql.createPool({
  //   // uri: "mysql://db:3306",
  //   host: "db",
  //   user: "root",
  //   password: "root",
  //   database: "mydb",
  //   port: 3306,
  //   waitForConnections: true,
  //   connectionLimit: 10,
  //   queueLimit: 0,
  // });

  // connection.connect();

  // return connection;
// };

module.exports = {
  createResult,
};
