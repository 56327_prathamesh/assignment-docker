const mysql = require("mysql2");

// const openConnection = () => {

  const openConnection = mysql.createPool({
    // uri: "mysql://db:3306",
    host: "db",
    user: "root",
    password: "root",
    database: "mydb",
    port: 3306,
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
  });

  module.exports = openConnection;